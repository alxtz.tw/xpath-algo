import {
  genCommonParentDescriptor,
  genXpathWithResult,
} from "../plugins/xpath.js";
import { genChildDescriptor } from "../plugins/genChildDescriptor";
import { readFileSync } from "fs";

describe("xpath algorithms", () => {
  const file = readFileSync("./e2e/pchomeFull.html");
  const exampleHtml = String(file);

  it.skip("example(1)", () => {
    const inputs = [
      "ul#MainSiteContainer> li:nth-child(1) > a",
      "ul#MainSiteContainer> li:nth-child(2) > a",
    ];

    const parentResult = genCommonParentDescriptor(inputs, exampleHtml);

    const result = genChildDescriptor(inputs, exampleHtml, parentResult);

    console.log(
      "result",
      "//" + genXpathWithResult(parentResult) + "//" + result
    );
  });

  it.skip("example(2)", () => {
    const inputs = [
      "ul#site_roadsign> li:nth-child(1) > a",
      "ul#site_roadsign> li:nth-child(2) > a",
    ];

    const parentResult = genCommonParentDescriptor(inputs, exampleHtml);

    const result = genChildDescriptor(inputs, exampleHtml, parentResult);

    console.log(
      "result",
      "//" + genXpathWithResult(parentResult) + "//" + result
    );
  });

  it.skip("example(3)", () => {
    const inputs = [
      "div#Block103_222Container > dl > dd:nth-child(1) > ul.clearfix > li:nth-child(1) > a",
      "div#Block103_222Container > dl > dd:nth-child(1) > ul.clearfix > li:nth-child(2) > a",
    ];

    const parentResult = genCommonParentDescriptor(inputs, exampleHtml);

    const result = genChildDescriptor(inputs, exampleHtml, parentResult);

    console.log(
      "result",
      "//" + genXpathWithResult(parentResult) + "//" + result
    );
  });

  it.skip("example(4)", () => {
    const inputs = [
      "ul#VenRaaSContainer > li:nth-child(1) > h6 > strong.s_price",
      "ul#VenRaaSContainer > li:nth-child(2) > h6 > strong.s_price",
    ];

    const parentResult = genCommonParentDescriptor(inputs, exampleHtml);

    const result = genChildDescriptor(inputs, exampleHtml, parentResult);

    console.log(
      "result",
      "//" + genXpathWithResult(parentResult) + "//" + result
    );
  });

  it.skip("[bug] example(5)", () => {
    const inputs = [
      "div#Block41Container ul.clearfix li li:nth-child(1) > a",
      "div#Block41Container ul.clearfix li li:nth-child(2) > a",
    ];

    const parentResult = genCommonParentDescriptor(inputs, exampleHtml);

    const result = genChildDescriptor(inputs, exampleHtml, parentResult);

    console.log("result", result);

    console.log(
      "result",
      "//" + genXpathWithResult(parentResult) + "//" + result
    );
  });

  it.skip("[wa] example(6)", () => {
    const inputs = [
      "ul#CutPriceContainer > li:nth-child(1) > h6",
      "ul#CutPriceContainer > li:nth-child(2) > h6",
    ];

    const parentResult = genCommonParentDescriptor(inputs, exampleHtml);

    console.log("p", parentResult);

    const result = genChildDescriptor(inputs, exampleHtml, parentResult);

    console.log("result", result);

    console.log(
      "result",
      "//" + genXpathWithResult(parentResult) + "//" + result
    );
  });

  it("example(7)", () => {
    const inputs = [
      "ul#AwardContainer > li:nth-child(1)",
      "ul#AwardContainer > li:nth-child(2)",
    ];

    const parentResult = genCommonParentDescriptor(inputs, exampleHtml);

    const result = genChildDescriptor(inputs, exampleHtml, parentResult);

    console.log("result", result);

    console.log(
      "result",
      "//" + genXpathWithResult(parentResult) + "//" + result
    );
  });
});
