import { parse } from "node-html-parser";

export function genChildDescriptor(childNodeQueries, bodyHtml, parentQuery) {
  const sanitized = bodyHtml.replace(/_id/g, "bypassissueunderscoreid");
  const root = parse(sanitized);

  const childNodes = [];

  childNodeQueries.forEach((queryStr) => {
    const eachChildNode = root.querySelector(queryStr);
    childNodes.push({ node: eachChildNode, replicaInTree: null });
  });

  childNodes.forEach((obj) => {
    const replicaInTree = markParentsWithReplica(obj.node);
    obj.replicaInTree = replicaInTree;
  });

  const anchorNode = childNodes.find((item) => item.replicaInTree !== null);

  if (anchorNode === undefined) {
    throw new Error("no replica layer to base from");
  }

  const childXPath = genChildXpathFromNode(
    anchorNode.node,
    anchorNode.replicaInTree
  );

  return childXPath;
}

function genChildXpathFromNode(anchorNode, replicaNode) {
  let descriptorList = [];

  let currNode = anchorNode;

  while (true) {
    if (currNode !== replicaNode) {
      const attrs = { ...currNode.attributes };

      Object.keys(attrs).forEach((key) => {
        if (currNode.rawAttrs.includes(`_${key}`)) {
          delete attrs[key];
        }

        if (key === "bypassissueunderscoreid") delete attrs[key];
      });

      descriptorList.push({
        tag: currNode.rawTagName,
        attributes: attrs,
      });

      currNode = currNode.parentNode;
    } else {
      const attrs = { ...currNode.attributes };

      Object.keys(attrs).forEach((key) => {
        if (currNode.rawAttrs.includes(`_${key}`)) {
          delete attrs[key];
        }

        if (key === "bypassissueunderscoreid") delete attrs[key];
      });

      descriptorList.push({
        tag: currNode.rawTagName,
        attributes: attrs,
      });

      break;
    }
  }

  const parentOfReplica = replicaNode.parentNode;
  const expectedLength = parentOfReplica.childNodes.filter(
    (node) => node.rawTagName
  ).length;

  let xpathStr = "";
  descriptorList = descriptorList.reverse();

  descriptorList.forEach((desc) => {
    let tagXpath = "";
    let attrXpath = "";

    const dryRunResult = parentOfReplica.querySelectorAll(desc.tag);
    if (dryRunResult.length === expectedLength) {
      tagXpath = desc.tag;
    } else {
      tagXpath = "*";
    }

    Object.entries(desc.attributes).forEach(([key, value]) => {
      const keyDryRunResult = parentOfReplica.querySelectorAll(`[${key}]`);
      const keyMatches = keyDryRunResult.length === expectedLength;

      const valDryRunResult = parentOfReplica.querySelectorAll(
        `[${key}="${value}"]`
      );
      const valMatches = valDryRunResult.length === expectedLength;

      let toPush = "";

      if (valMatches) {
        toPush += `@${key}='${value}'`;
      } else if (keyMatches) {
        toPush += `@${key}`;
      }

      if (attrXpath != "") {
        attrXpath += " and ";
      }

      attrXpath += toPush;
    });

    if (xpathStr !== "") {
      xpathStr += "/";
    }

    xpathStr += `${tagXpath}`;

    if (attrXpath !== "") {
      xpathStr += `[${attrXpath}]`;
    }
  });

  return xpathStr;
}

function markParentsWithReplica(node) {
  let inputNode = node;

  let hasAnyReplica = null;

  while (true) {
    if (inputNode.parentNode === null) {
      break;
    }

    let inputSibs = inputNode.parentNode.childNodes;
    inputSibs = inputSibs.filter((sibNode) => {
      return sibNode.rawTagName && sibNode !== inputNode;
    });

    const similarity = getSimilarity(inputNode, inputSibs);

    const simScore = similarity * 30;
    const lengthScore = inputSibs.length * 8 > 30 ? 30 : inputSibs.length * 5;

    const finalScore = simScore + lengthScore;

    inputNode.isReplicaLayer = finalScore > 40;

    if (inputNode.isReplicaLayer && hasAnyReplica === null) {
      hasAnyReplica = inputNode;
    }

    inputNode = inputNode.parentNode;
  }

  return hasAnyReplica;
}

function getSimilarity(node, siblingNodes) {
  const nodeAttrs = node.attributes;
  const nodeAttrsLength = Object.keys(node.attributes).length;
  const nodeRawTagName = node.rawTagName;

  const filteredSiblings = siblingNodes.filter((sibNode) => {
    return sibNode.rawTagName !== undefined && sibNode !== node;
  });

  let score = 0;

  filteredSiblings.forEach((sib) => {
    if (sib.rawTagName === nodeRawTagName) {
      score += 0.4;
    }

    const sibAttrs = sib.attributes;

    Object.keys(nodeAttrs).forEach((attrName) => {
      if (sibAttrs[attrName] === undefined) {
        return;
      } else {
        score += 0.1 / nodeAttrsLength;
      }

      if (sibAttrs[attrName] === nodeAttrs[attrName]) {
        score += 0.5 / nodeAttrsLength;
      }
    });
  });

  if (score === 0 || filteredSiblings.length === 0) {
    return 0;
  }

  return score / filteredSiblings.length;
}
