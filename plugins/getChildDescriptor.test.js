import { genChildDescriptor } from "./genChildDescriptor";
import { readFileSync } from "fs";

describe("xpath algorithms", () => {
  const file = readFileSync("./plugins/pchomeExample.html");
  const exampleHtml = String(file);

  it("should generate common child structure for <a>24h購物</a> & <a>購物車</a>", () => {
    const result = genChildDescriptor(
      [
        "ul#MainSiteContainer> li:first-child > a", // this is the replica
        "ul#more > li:first-child > a",
      ],
      exampleHtml,
      [
        ["div", 'class="block_H"'],
        ["div", 'class="Ht"'],
        ["div", 'class="site_nav"'],
      ]
    );
    expect(result).toEqual("li/a[@href]");
  });

  it("should generate common child structure for <a>24h購物</a>", () => {
    const result = genChildDescriptor(
      ["ul#site_roadsign > li:first-child > a", "ul#more > li:first-child > a"],
      exampleHtml,
      [
        ["div", 'class="block_H"'],
        ["div", 'class="Hm"'],
        ["div", 'class="roadsign_nav"'],
      ]
    );

    expect(result).toEqual(
      // "li[contains(@class, 'overlay_roadsign')]/a[@href and contains(@class, 'sign')]"
      // "li[@class='overlay_roadsign']/a[@href@class='sign']"
      "li/a[@href@class='sign']"
    );
  });

  // it("should generate common child structure for <a>書店</a>, <a>3C</a>, <a>周邊</a>", () => {
  //   expect(result).toEqual(
  //     "li[contains(@class, 'overlay_roadsign')]/a[contains(@class, 'sign')]"
  //   );
  // });

  // it("should generate common child structure for <a>五倍券振興專區</a> & <a>3C</a>", () => {
  //   expect(result).toEqual(
  //     "li[contains(@class, 'overlay_roadsign')]/a[contains(@class, 'sign')]"
  //   );
  // });
});
