import { parse } from "node-html-parser";

// const exampleHtml = readFileSync("./plugins/pchomeExample.html");

/*
type CssQueryStr = string
type HtmlStr = string

type TagName = string
type Attrs = string
type ParentEle = [TagName, Attrs]

genCommonParentDescriptor(
  childNodeQueries: CssQueryStr[],
  bodyHtml: HtmlStr
): ParentEle[]
*/

export function genCommonParentDescriptor(childNodeQueries, bodyHtml) {
  const sanitized = bodyHtml.replace(/_id/g, "bypassunderscoreid");
  const root = parse(sanitized);

  const childNodes = [];

  childNodeQueries.forEach((queryStr) => {
    const eachChildNode = root.querySelector(queryStr);
    childNodes.push(eachChildNode);
  });

  let circuitBreaker = 0;

  let parentNodeList = [];

  childNodes.forEach((node) => {
    parentNodeList.push([]);
  });

  childNodes.forEach((node, index) => {
    let currNode = node;

    while (true) {
      if (currNode.parentNode === null) break;

      parentNodeList[index] = [currNode.parentNode, ...parentNodeList[index]];
      currNode = currNode.parentNode;
    }
  });

  let commonPatternAccu = [];
  let index = 0;

  while (true) {
    {
      if (circuitBreaker > 1000) break;
      circuitBreaker += 1;
    }

    if (index >= parentNodeList[0].length) {
      break;
    }

    const anchorItem = parentNodeList[0][index];

    const allParentIsSame = parentNodeList.every((parentList) => {
      return parentList[index] === anchorItem;
    });

    if (allParentIsSame) {
      if (anchorItem.rawTagName !== null) {
        commonPatternAccu = [
          ...commonPatternAccu,
          [anchorItem.rawTagName, anchorItem.rawAttrs],
        ];
      }
      index += 1;
      continue;
    } else {
      break;
    }
  }

  return commonPatternAccu;
}

export function genXpathWithResult(patternList) {
  let xpathStr = "";

  patternList.forEach(([tag, attrs]) => {
    if (xpathStr !== "") {
      xpathStr += "/";
    }

    let expression = "";

    let attrsRaw = attrs.split('" ');
    if (attrsRaw.length > 1) attrsRaw = attrsRaw.map((s) => s + '"');

    attrsRaw.forEach((raw) => {
      if (raw === "") {
        return;
      }
      const key = raw.split("=")[0];
      const val = raw.split("=")[1].replace(/"/g, "");

      if (key !== "bypassunderscoreid") {
        if (expression !== "") {
          expression += ` and `;
        }
        expression += `@${key}='${val}'`;
      }
    });

    xpathStr += `${tag}`;

    if (expression !== "") {
      xpathStr += `[${expression}]`;
    }
  });

  return xpathStr;
}
