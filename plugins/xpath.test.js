/* eslint-disable no-undef */
import { genCommonParentDescriptor } from "./xpath.js";
import { readFileSync } from "fs";

describe("xpath algorithms", () => {
  const file = readFileSync("./plugins/pchomeExample.html");
  const exampleHtml = String(file);

  it("should find common parent when querying with <a>24h購物</a> & <a>購物車</a>", () => {
    const result = genCommonParentDescriptor(
      [
        "ul#more > li:first-child > a",
        "ul#MainSiteContainer> li:first-child > a",
      ],
      exampleHtml
    );

    expect(result).toEqual([
      ["div", 'class="block_H"'],
      ["div", 'class="Ht"'],
      ["div", 'class="site_nav"'],
    ]);
  });

  it("should find common parent when querying with <a>24h購物</a> & <a>書店</a>", () => {
    const result = genCommonParentDescriptor(
      [
        "ul#MainSiteContainer> li:nth-child(1) > a",
        "ul#MainSiteContainer> li:nth-child(2) > a",
      ],
      exampleHtml
    );

    expect(result).toEqual([
      ["div", 'class="block_H"'],
      ["div", 'class="Ht"'],
      ["div", 'class="site_nav"'],
      ["ul", 'class="main_site" id="MainSiteContainer"'],
    ]);
  });

  it("should find common parent when querying with <a>書店</a>, <a>3C</a>, <a>周邊</a>", () => {
    const result = genCommonParentDescriptor(
      [
        "ul#MainSiteContainer> li:nth-child(2) > a",
        "ul#site_roadsign> li:nth-child(1) > a",
        "ul#site_roadsign> li:nth-child(2) > a",
      ],
      exampleHtml
    );

    expect(result).toEqual([["div", 'class="block_H"']]);
  });

  it("should find common parent when querying with <a>五倍券振興專區</a> & <a>3C</a>", () => {
    const result = genCommonParentDescriptor(
      [
        "ul#site_roadsign> li:nth-child(1) > a",
        "ul.site_hot> li:nth-child(1) > a",
      ],
      exampleHtml
    );

    expect(result).toEqual([
      ["div", 'class="block_H"'],
      ["div", 'class="Hm"'],
      ["div", 'class="roadsign_nav"'],
    ]);
  });
});
